local firstspawn = 0
local activehud = true

local localfood = "MAJ en attente"
local localwater = "MAJ en attente"
local localwc = "MAJ en attente"
local initialized = false


AddEventHandler('playerSpawned', function(spawn)
	if firstspawn == 0 then
		firstspawn = 1
	else
		TriggerServerEvent('player_state:setdefaultneeds')
	end
	Citizen.Wait(5000)
	initialized = true
end)

AddEventHandler('player_state:chekwnews', function(bool)
	activehud = bool
end)

RegisterNetEvent('player_state:death')
AddEventHandler('player_state:death', function()
	--SetEntityHealth(GetPlayerPed(-1), 0)--retirer pour le server test
end)

-- FOOD
RegisterNetEvent('player_state:set')
AddEventHandler('player_state:set', function(state)
	localfood = state.food
	localwater = state.water
	localwc = state.needs
	
	print(tostring("food " .. localfood), tostring("localwater " .. localwater),tostring("localwc " .. localwc))
	
	SendNUIMessage({ state = json.encode({food = state.food, water = state.water, needs = state.needs, status = true})
	})
end)

-- EMOTES
RegisterNetEvent('player_state:drink')
AddEventHandler('player_state:drink', function()
	ped = GetPlayerPed(-1)
	if ped then
		Citizen.CreateThread(function()
			RequestAnimDict('amb@world_human_drinking_fat@beer@male@idle_a')
		    local pedid = PlayerPedId()
			TaskPlayAnim(pedid, 'amb@world_human_drinking_fat@beer@male@idle_a', 'idle_a', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(5000)
			ClearPedTasks(ped)
		end)
	end
end)

RegisterNetEvent('player_state:eat')
AddEventHandler('player_state:eat', function()
	ped = GetPlayerPed(-1)
	if ped then
		Citizen.CreateThread(function()
			RequestAnimDict('amb@code_human_wander_eating_donut@male@idle_a')
		    local pedid = PlayerPedId()
			TaskPlayAnim(pedid, 'amb@code_human_wander_eating_donut@male@idle_a', 'idle_c', 8.0, -8, -1, 16, 0, 0, 0, 0)
			Citizen.Wait(5000)
			ClearPedTasks(ped)
		end)
	end
end)

RegisterNetEvent('player_state:pee')
AddEventHandler('player_state:pee', function()
	ped = GetPlayerPed(-1)
	local hashSkin = GetHashKey("mp_m_freemode_01")
	if IsPedInAnyVehicle(GetPlayerPed(-1), true) == false then
		if ped then
			if(GetEntityModel(GetPlayerPed(-1)) ~= hashSkin) then
				Citizen.CreateThread(function()
					RequestAnimDict('missfbi3ig_0')
					local pedid = PlayerPedId()
					TaskPlayAnim(pedid, 'missfbi3ig_0', 'shit_loop_trev', 8.0, 8, -1, 0, 0, 0, 0, 0)
					ClearPedTasks(ped)
				end)
			else
				Citizen.CreateThread(function()
					RequestAnimDict('misscarsteal2peeing')
					local pedid = PlayerPedId()
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_intro', 8.0, -8, -1, 0, 0, 0, 0, 0)
					Citizen.Wait(GetAnimDuration('misscarsteal2peeing', 'peeing_intro'))
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_loop', 8.0, -8, -1, 0, 0, 0, 0, 0)
					Citizen.Wait(GetAnimDuration('misscarsteal2peeing', 'peeing_loop'))
					TaskPlayAnim(pedid, 'misscarsteal2peeing', 'peeing_outro', 8.0, -8, -1, 0, 0, 0, 0, 0)
					ClearPedTasks(ped)
				end)
			end
		end
	else
		--TriggerEvent("es_freeroam:notify", "CHAR_MP_STRIPCLUB_PR", 1, "Mairie", false, "ca serai pas mieux en dehors du vehicule?")
	end
end)

local activehud = true
local hudactivationnotbool = false

RegisterNetEvent('hud:active')
AddEventHandler('hud:active',function()
	
	if not hudactivationnotbool then
		hudactivationnotbool = true
		activehud = false
	else
		hudactivationnotbool = false
		activehud = true
	end
end)

Citizen.CreateThread(function()
	while true do
		Citizen.Wait(500)
		if IsPauseMenuActive() or not activehud then
			if not pauseactivated then
				SendNUIMessage({ setDisplay = true,	opacityIcon = 0, state = json.encode({food = localfood, water = localwater, needs = localwc, status = true})})
			end
			pauseactivated = true
		elseif pauseactivated then
			SendNUIMessage({ setDisplay = true,	opacityIcon = 1, state = json.encode({food = localfood, water = localwater, needs = localwc, status = true})})
			pauseactivated = false
		end
	end
end)
Citizen.CreateThread(function()
	while true do
		if initialized then
			TriggerServerEvent('player_state:check')
			Citizen.Wait(120000) -- 120000
		else
			Citizen.Wait(10)
			TriggerServerEvent('player_state:check')
			initialized = true
		end
	end
end)